resource "google_compute_instance" "vm-from-tf" {
  name         = "vm-from-tf"
  zone         = "us-central1-a"
  machine_type = "n1-standard-2"

  allow_stopping_for_update = true

  network_interface {
    network    = "default"
    
  }

  boot_disk {
    initialize_params {
      image = "debian-9-stretch-v20210916"
      size  = 35

    }
    #auto_delete = false
  }

  labels = {
    "env" = "tfleaning"
  }
}
